import 'package:flutter/material.dart';
import 'package:my_app/constants/routes.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Dashboard')),
      body:Center(
        child: Column(
            children: [
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(featureOneRoute);
                  },
                  child: const Text('Feature page one')),
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(featureTwoRoute);
                  },
                  child: const Text('Feature page two')),
              
            ],
          ),
        ) ,
        floatingActionButton: FloatingActionButton(onPressed: () {
          Navigator.of(context).pushNamed(userProfileRoute);
        },
        tooltip: 'Edit Profile',
        child: const Icon(Icons.add),),
    );
  }
}
