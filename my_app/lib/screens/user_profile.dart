import 'package:flutter/material.dart';


class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  late final TextEditingController _name;
  late final TextEditingController _surname;
  late final TextEditingController _username;
  late final TextEditingController _number;
  late final TextEditingController _city;
  late final TextEditingController _description;

  @override
  void initState() {
    _name = TextEditingController();
    _surname = TextEditingController();
    _username = TextEditingController();
    _number = TextEditingController();
    _city = TextEditingController();
    _description = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _name.dispose();
    _surname.dispose();
    _username.dispose();
    _number.dispose();
    _city.dispose();
    _description.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Edit Profile')),
      body: Column(
        children: [
          TextField(
            controller: _name,
            enableSuggestions: false,
            autocorrect: false,
            decoration: const InputDecoration(hintText: "Edit Name"),
          ),
          TextField(
            controller: _surname,
            enableSuggestions: false,
            autocorrect: false,
            decoration: const InputDecoration(hintText: "Edit Lastname"),
          ),
          TextField(
            controller: _username,
            enableSuggestions: false,
            autocorrect: false,
            decoration: const InputDecoration(hintText: "Edit Username"),
          ),
           TextField(
            controller: _number,
            enableSuggestions: false,
            autocorrect: false,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(hintText: 'Edit Phone Number'),
          ),
          TextField(
            controller: _city,
            enableSuggestions: true,
            autocorrect: true,
            decoration: const InputDecoration(hintText: "Edit City"),
          ),
          TextField(
            controller: _city,
            enableSuggestions: true,
            autocorrect: true,
            maxLines: null,
            keyboardType:TextInputType.multiline,
             decoration: const InputDecoration(hintText: "Edit your bio"),
          ),
         OutlinedButton(onPressed: (){}, child: const Text('Save Changes'))
        ],
      ),
    );
  }
}
