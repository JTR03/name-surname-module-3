import 'package:flutter/material.dart';


class FeaturePageTwo extends StatelessWidget {
  const FeaturePageTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Feature Page Two')),
    );
  }
}
